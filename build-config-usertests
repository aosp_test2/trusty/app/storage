# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file lists userspace tests

[
    # userspace tests using storage available at early boot
    needs(
        [
            # init->check->clean is a side-effectful test chain
            # so we use a composite test to preserve order
            compositetest(
                name="com.android.storage-unittest.tp-init-check-clean",
                sequence=[
                    porttest("com.android.storage-unittest.tp.init"),
                    reboot(),
                    porttest("com.android.storage-unittest.tp.check"),
                    porttest("com.android.storage-unittest.tp.clean"),

                ]
            ),
            porttest("com.android.storage-unittest.tp", timeout=(60 * 15)),
            compositetest(
                name="com.android.storage-unittest.tdea-init-check-clean",
                sequence=[
                    porttest("com.android.storage-unittest.tdea.init"),
                    reboot(),
                    porttest("com.android.storage-unittest.tdea.check"),
                    porttest("com.android.storage-unittest.tdea.clean"),
                ]
            ),
            porttest("com.android.storage-unittest.tdea", timeout=(60 * 15)),
        ],
        storage_boot=True,
    ),

    # Storage tests which require a nonsecure file
    # These need Android or a similar resource to run, simple RPMB
    # in testrunner is insufficient.
    needs(
        [
            porttest("com.android.storage-unittest.nsp", timeout=(60 * 15)),
            porttest("com.android.storage-unittest.td"),
            porttest("com.android.storage-unittest.tdp", timeout=(60 * 15)),
        ],
        storage_full=True,
    ),
]
